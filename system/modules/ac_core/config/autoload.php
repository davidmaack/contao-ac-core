<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package Ac_core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'AcCoreAjax'    => 'system/modules/ac_core/classes/AcCoreAjax.php',
	'AutoCompleter' => 'system/modules/ac_core/classes/AutoCompleter.php',
));
